#!/bin/zsh

# Set a random wallpaper for the next loggin
touch $HOME/.wallpaper/$(\ls ~/.wallpaper | head -n $(($RANDOM % $(\ls ~/.wallpaper | wc -l) + 1)) | tail -n 1)

# Test connection, if yes, run updates below
CONNECTED=false
echo "\n    Test connection to network :\n"
sleep 1
if ping -c 1 -W 5 archlinux.org; then
    CONNECTED=true
else
    echo "\n    Not connected, here’s available networks :\n"
    nmcli --fields IN-USE,SSID,BARS,RATE,SECURITY --colors yes --pretty device wifi list | head -n 27
    echo "\n    Continue anyway ? (y/N)"
    read ANSWER
    if [ ! -v $ANSWER ] && [ $ANSWER = "y" ]; then
        CONNECTED=true
    fi
fi

# Update the task early to display them ASAP
if $CONNECTED; then
    echo "\n    Update the tasks :\n"
        git -C $HOME/.task/ pull
fi


swaymsg move position 5 120 # Arrange layout in convenient way and display tasks
nohup \
    alacritty --class task --working-directory $HOME -e \
    zsh -ic "sleep 1 && task calendar && task  && zsh" \
    &> /dev/null & disown

# Launch flashfocus (window focusing animation)
# kill $(pgrep flashfocus)
# flashfocus -v ERROR & disown

# Update documents and eventually packages
if $CONNECTED; then
    echo "\n    Update the secrets :\n"
        pass git pull
    echo "\n    Update the config :\n"
        git --git-dir=$HOME/.bare/ --work-tree=$HOME pull

    echo "\n    Update the documents :"
    echo "\n      BUT :\n"
        git -C $HOME/doc/but/ pull
    echo "\n      Projects :\n"
        git -C $HOME/project/ pull

    echo "\n    Let's get up to date :\n"
        if [ $(( $(date +%u) % 3 )) -eq 0 ]; then # upgrade systm on Wed & Sat
            # sudo pacman -Syu --noconfirm
            # yay -Sua --noconfirm
            yay -Syu --noconfirm
        else
            echo "Not a day to upgrade the system"
        fi
fi

# Verify if some prefered settings are set and warn user if not
echo "\n    Verify some settings :"
echo "        dash as default shell:"
    if (ls -l /bin/sh | grep bash); then
        sudo ln -svf /usr/bin/dash /usr/bin/sh
    else
        echo "        dash is already the default shell"
    fi
echo "        alacritty as nvim.desktop terminal:"
    if (grep alacritty /usr/share/applications/nvim.desktop); then
        echo "        alacritty is already the nvim terminal"
    else
        echo "        WARNING, alacritty is not the nvim terminal, edit /usr/share/applications/nvim.desktop !"
    fi

# Display info on this system and leave the user with interactive shell
exec zsh -ic "echo '\n\n' && neofetch && cd ~/doc && ls && zsh"
