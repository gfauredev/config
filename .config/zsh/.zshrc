# GF’s ~/.config/zsh/.zshrc

# unrelated commands
## delete annoying stuff
[ -d ~/Downloads ] && rmdir ~/Downloads # standard XDG Downloads directory
[ -d ~/Desktop ] && rmdir ~/Desktop # standard XDG Desktop directory

# source aliases
source $XDG_CONFIG_HOME/alias.sh
source $XDG_CONFIG_HOME/functions.sh

# history tweaking
## storage
HISTFILE=$XDG_DATA_HOME/zhistory
HISTSIZE=100000
SAVEHIST=100000
## search
# bindkey "^[[A" history-beginning-search-backward
# bindkey "^[[B" history-beginning-search-forward

# options
setopt autocd extendedglob nomatch menucomplete share_history correct

# cursor shape
echo -ne '\e[6 q' # use beam shape cursor on startup
preexec() { echo -ne '\e[6 q' ;} # use beam shape cursor for each new prompt 

# disable beeping
unsetopt BEEP

# completion
fpath+=~/.config/zsh/func
zmodload zsh/complist
autoload -Uz compinit && compinit -i && _comp_options+=(globdots)
zstyle ':completion:*' completer  _complete _approximate _expand_alias
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# vi mode
bindkey -v
## command mode
bindkey -M vicmd 'c' vi-backward-char
# bindkey -M vicmd 't' vi-down-line-or-history
# bindkey -M vicmd 's' vi-up-line-or-history
bindkey -M vicmd 't' history-beginning-search-forward
bindkey -M vicmd 's' history-beginning-search-backward
bindkey -M vicmd 'r' vi-forward-char

# tab complete menu
bindkey -M menuselect 'c' vi-backward-char
bindkey -M menuselect 't' vi-down-line-or-history
bindkey -M menuselect 's' vi-up-line-or-history
bindkey -M menuselect 'r' vi-forward-char

# colors
autoload -Uz colors && colors

# evals
# eval "$(thefuck --alias)"
eval "$(starship init zsh)"

# source plugins
## z or zoxide
source /usr/share/z/z.sh
## fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
## zsh autosuggestions and syntax highlighting community pluggins
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /home/gf/.config/broot/launcher/bash/br
