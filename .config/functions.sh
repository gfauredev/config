#!/bin/sh

# open a new terminal within a particular directory
function term {
    CMD=$SHELL
    if [ "$#" -eq 0 ]; then OPT="--working-directory=$PWD"; fi
    if [ -n "$1" ]; then
        [ "$#" -gt 1 ] && CMD="$1"
        [ "$#" -eq 1 ] && OPT="--working-directory=$1"
        # if (which "$1"); then
        #     CMD="$1"
        # else
        #     OPT="--working-directory=$1"
        # fi
    fi
    if [ -n "$2" ]; then
        CMD="$1"
        OPT="--working-directory=$2"
        # special cases
        if [[ "$2" == "menu" ]]; then OPT='--class="menu"'; fi
    fi
    alacritty $OPT -e $SHELL -ic $CMD & disown
}
# search through command list ~/.applist with fzf and launch
# function launch {
#     LIST=$(cat ~/.applist)
#     SELECT="$(echo $LIST | fzf --preview 'if [[ {} == *".desktop" ]]; then chafa --size 42x42 $(fd -g $(echo $(\rg Icon /usr/share/applications/{}|cut -d"=" -f2 | head -n 1).\*g) /usr/share | head -n 1 | tr -d "\n"); else man {}; fi')"
#     if [[ "$SELECT" == *".desktop" ]]; then
#         CMD="/usr/bin/$(\rg Exec /usr/share/applications/$SELECT|cut -d'=' -f2|head -n 1|tr '/' '\n'|tail -n 1|sed 's/%u//I;s/%f//I')"
#         # echo launched = $CMD
#         zsh -ic "$CMD & disown && sleep 5"
#     elif [[ "$SELECT" == *".wine" ]]; then
#         CMD="wine start 'C:\\$(\rg $(echo $SELECT|cut -d'.' -f1) ~/.wineapps|cut -d' ' -f2-)'"
#         # echo launched = $CMD
#         zsh -ic "$CMD & disown && sleep 5"
#     elif [ -n "$SELECT" ]; then
#         zsh -ic "term 'man $SELECT; zsh' ~"
#     fi
# }
# open (in background if needed)
function open {
    HANDLER="xdg-open"
    if [ "$#" -eq 0 ]; then
        TARGET=$(fzf --preview 'if [[ $(xdg-mime query filetype {} | cut -d'/' -f1) == "image" ]]; then chafa {}; else exa -l {} && bat {} --color=always; fi')
    fi
    if [ "$#" -eq 1 ]; then
        if [[ "$1" == "." ]]; then
            TARGET="."
        elif (which "$1"); then
            TARGET=$(fzf --preview 'if [[ $(xdg-mime query filetype {} | cut -d'/' -f1) == "image" ]]; then chafa {}; else exa -l {} && bat {} --color=always')
            HANDLER="$1"
        else
            TARGET="$1"
        fi
    fi
    if [ -n "$2" ]; then
        HANDLER="$1"
        TARGET="$2"
    fi
    EXT=$(echo $TARGET | tr '.' '\n' | tail -n 1)
    if [[ $EXT == "gpr" ]]; then
        HANDLER="gnatstudio"
    fi
    if [ -n "$HANDLER" ] && [ -n "$TARGET" ]; then
        zsh -ic "$HANDLER '$TARGET' & disown"
    fi
}
# small functions
function m {
    mkdir -p "$1" && cd "$1"
}
function boot {
    efibootmgr -v
    if [ -n "$1" ]; then
        sudo efibootmgr -n "$1"
        echo "Reboot now ? y/N :"
        read ANSWER
        if [[ $ANSWER == "y" ]]; then
            sudo reboot
        else
            echo "Shutdown now ? y/N :"
            read ANSWER
            if [[ $ANSWER == "y" ]]; then
                sudo shutdown now
            fi
        fi
    fi
}
# function snapshot {
#     DIR=$(echo "$*" | tr -d ' ','/','-' | tr 'r' 'R')
#     sudo btrfs subvolume snapshot "$@" /.snapshots/$DIR/$(date +%d-%m-%Y_%H:%M:%S)
# }
function preview {
    if [ -n "$1" ]; then
        DIR=$1
    else
        DIR=$PWD
    fi
    if (cd $DIR && zola check); then
        term 'zola serve' $DIR
        xdg-open http://localhost:1111
    else
        term 'browser-sync -f . -s .' $DIR
    fi
}
function cdf {
    DIR=$(fd . -t d| fzf --preview 'echo {}; dust {}')
    cd "$DIR"
}
# function taskgitwrap {
#     task "$*" && git -C $HOME/.task/ commit -am "AUTO COMMIT from $(hostnamectl hostname)" && git -C $HOME/.task/ push
# }
# function winelaunch {
#     j=0
#     for i in $(cat ~/.wineapps); do
#         j=$((j+1))
#         echo $j: $i
#     done
#
#     echo Choose an app by number :
#     read answer
#
#     j=0
#     for i in $(cat ~/.wineapps); do
#         j=$((j+1))
#         if [[ $j -eq $answer ]]; then
#             app="$i"
#         fi
#     done
#
#     wine start "C:\\$(echo $app)"
# }
function loop {
    if [ -n "$2" ]; then
        COOLDOWN="$2"
    else
        COOLDOWN="30"
    fi
    while "$1"; do sleep $COOLDOWN && clear; done
}
