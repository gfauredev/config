#!/bin/sh
DEST="file:///run/media/$USER/$USER-back"
dirlist=""

if [ "$1" = "all" ]; then
    for dir in $(echo "archive book doc perso project .config .fh .gnupg .password-store .script .task .wallpaper"); do
        dirlist=$dirlist --include "$HOME/$dir"
    done
else
    for dir in $(echo $*); do
        dirlist=$dirlist --include "$HOME/$dir"
    done
fi

duplicity --full-if-older-than 6M --copy-links --allow-source-mismatch $dirlist --exclude "**" --verbosity 8 $HOME $DEST
